## [2.2.2](https://gitlab.com/depixy/env/compare/v2.2.1...v2.2.2) (2020-12-15)


### Bug Fixes

*  default value typings ([2355da8](https://gitlab.com/depixy/env/commit/2355da85ef1b4482a9635299a75c3f09a0cebdf3))

## [2.2.1](https://gitlab.com/depixy/env/compare/v2.2.0...v2.2.1) (2020-12-15)


### Bug Fixes

* remove unused types ([d23971e](https://gitlab.com/depixy/env/commit/d23971e6d44450222b5f1c00fc288afb972002e8))

# [2.2.0](https://gitlab.com/depixy/env/compare/v2.1.0...v2.2.0) (2020-12-15)


### Features

* allow different type of default value ([a7b679b](https://gitlab.com/depixy/env/commit/a7b679b28d7969ddf150538882a67f548d941cb4))

# [2.1.0](https://gitlab.com/depixy/env/compare/v2.0.0...v2.1.0) (2020-12-15)


### Features

* add default parse options ([d650ea4](https://gitlab.com/depixy/env/commit/d650ea48b981e793aaad18579d7df967dc591792))
* update parse options ([95728c5](https://gitlab.com/depixy/env/commit/95728c594645e4e8982309281dd634f7d2848129))

# [2.0.0](https://gitlab.com/depixy/env/compare/v1.0.0...v2.0.0) (2020-12-15)


### Code Refactoring

* update interface ([bb5fd9f](https://gitlab.com/depixy/env/commit/bb5fd9f592ec3747ce694bb45aecb8d998890d2b))


### BREAKING CHANGES

* new interface
