import _ from "lodash";

const toInt = (value: string): number => parseInt(value, 10);
const toFloat = (value: string): number => parseFloat(value);
const toBool = (value: string): boolean => value.toLowerCase() === "true";
const toString = (value: string): string => value;

type ParseOption<T> = { defaultValue: T } | { assert: true };

const defaultOrThrow = <T>(key: string, opts: ParseOption<T>): T => {
  if ("assert" in opts) {
    throw new Error(`${key} is not defined.`);
  }
  return opts.defaultValue;
};

const parseEnv = <T>(parseFn: (value: string) => T) => <U>(
  key: string,
  opts: ParseOption<U> = { assert: true }
): T | U => {
  if (!_.has(process.env, key)) {
    return defaultOrThrow(key, opts);
  }
  const value = process.env[key];
  if (_.isNil(value)) {
    return defaultOrThrow(key, opts);
  }
  return parseFn(value);
};

export const env = {
  int: parseEnv(toInt),
  float: parseEnv(toFloat),
  bool: parseEnv(toBool),
  string: parseEnv(toString)
};
