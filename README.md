# @depixy/env

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Storage middleware for Depixy.

## Installation

```
npm i @depixy/env
```

[license]: https://gitlab.com/depixy/env/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@depixy/env
[pipelines]: https://gitlab.com/depixy/env/pipelines
[pipelines_badge]: https://gitlab.com/depixy/env/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@depixy/env
[npm_badge]: https://img.shields.io/npm/v/@depixy/env/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
